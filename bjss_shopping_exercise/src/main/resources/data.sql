INSERT INTO Product (name, price) VALUES
  ('Soup', 0.65),
  ('Bread', 0.80),
  ('Milk', 1.3),
  ('Apples', 1),
  ('Oranges', 2);

INSERT INTO Discount (discount_type, product_id) VALUES
  ('DIR', 4),
  ('ACC', 1);
  
INSERT INTO Discount_Direct (id, percentage) VALUES
  (1, 0.1);
  
INSERT INTO Discount_By_Accumulation (id, product_for_disc_min_num, prod_discounted_id, prod_for_prod_relation, percentage) VALUES
  (2, 2, 2, 1, 0.5);