CREATE TABLE Product (
  id          IDENTITY PRIMARY KEY,
  name 		  VARCHAR NOT NULL,
  price		  DECIMAL NOT NULL);

CREATE TABLE Discount (
  id          IDENTITY PRIMARY KEY,
  discount_type VARCHAR NOT NULL,
  product_id  BIGINT NOT NULL);
  
CREATE TABLE Discount_Direct (
  id          BIGINT NOT NULL,
  percentage  REAL NOT NULL);
  
CREATE TABLE Discount_By_Accumulation (
  id          BIGINT NOT NULL,
  product_for_disc_min_num INTEGER NOT NULL,
  prod_discounted_id  BIGINT NOT NULL,
  prod_for_prod_relation INTEGER NOT NULL,
  percentage  REAL NOT NULL);