package com.shopping.basket;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shopping.discount.entity.DiscountByAccumulation;
import com.shopping.discount.entity.DiscountDirect;
import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;
import com.shopping.discount.service.impl.DiscountServiceImpl;
import com.shopping.product.entity.Product;
import com.shopping.product.service.impl.ProductServiceImpl;

@Component
public class Basket {

	@Autowired
	private ProductServiceImpl productService;
	@Autowired
	private DiscountServiceImpl discountService;

	private List<Product> products;
	private BigDecimal subTotal;
	private BigDecimal total;
	private StringBuilder appliedDiscounts;

	public Basket() {
		this.products = new LinkedList<Product>();
		this.subTotal = new BigDecimal(0);
		this.total = new BigDecimal(0);
		this.appliedDiscounts = new StringBuilder();
	}

	public void addProduct(String productName) {
		Product prod = productService.findByName(productName);

		if (prod != null) {
			products.add(prod);
		}
	}

	public List<Product> getProductsInBasket() {
		return this.products;
	}

	public void calculateTotals() {
		// the identity is set as BigDecimal.ZERO as it's the initial value of the accumulator operation
		setSubTotal(products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add));

		applyDiscounts();

		setTotal(products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add));
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getSubTotal() {
		return this.subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	
	public String getAppliedDiscounts() {
		return this.appliedDiscounts.toString();
	}
	
	public void clearBasket() {
		this.products.clear();
	}

	// Applies the product discounts to the total price
	private void applyDiscounts() {
		List<Discount> discs;
		List<Product> distinctProducts = products.stream().distinct().collect(Collectors.toList());
		List<Product> productsAcc;
		BigDecimal temp;
		BigDecimal discount;

		// The direct discounts are implemented first
		for (Product p : products) {
			discs = discountService.findByProductId(p.getId()).stream().
					filter(ds -> ds.getDiscountType().equals(DiscountType.DIRECT.getDiscType())).
					collect(Collectors.toList());

			for (Discount d : discs) {
				discount = p.getPrice().multiply(new BigDecimal(((DiscountDirect) d).getPercentage()),
						MathContext.DECIMAL32).setScale(2, RoundingMode.CEILING);
				temp = p.getPrice().subtract((p.getPrice()
						.multiply(new BigDecimal(((DiscountDirect) d).getPercentage()), MathContext.DECIMAL32)));
				p.setPrice(temp.setScale(2, RoundingMode.CEILING));
				appliedDiscounts.append(
						(p.getName() + " " + ((DiscountDirect) d).getPercentage()*100 + "% off: -" + discount + "\n"));
			}
		}

		/* 
		 * Applying the accumulation discounts requires that no repeated discounts are applied,
		 * thus the list of distinct products
		 */
		for (Product bp : distinctProducts) {
			discs = discountService.findByProductId(bp.getId()).stream().
					filter(ds -> ds.getDiscountType().equals(DiscountType.ACCUMULATION.getDiscType())).
					collect(Collectors.toList());

			for (Discount d : discs) {
				DiscountByAccumulation discByAcc = (DiscountByAccumulation) d;

				/* 
				 * The number of applicable discounts is calculated based on
				 *  - Total number of the products that trigger the discount that are in the basket 
				 *  - Minimum number of products to trigger the accumulation discount
				 *  - The relation between the product that triggers the accumulation and how many products show be affected 
				 */
				long totalDiscProducts 
				= ((products.stream().filter(prod -> prod.getId() == discByAcc.getProductId()).count())
						/discByAcc.getProductForDiscMinNum()) * discByAcc.getProdForProdRelation();

				productsAcc = products.stream().filter(prod -> prod.getId() == discByAcc.getProdDiscountedId())
				.collect(Collectors.toList());
				
				for (int i = 0; i < totalDiscProducts && i < productsAcc.size(); i++) {
					discount = productsAcc.get(i).getPrice()
							.multiply(new BigDecimal(((DiscountByAccumulation) d).getPercentage()), MathContext.DECIMAL32)
							.setScale(2, RoundingMode.CEILING);
					temp = productsAcc.get(i).getPrice().subtract((productsAcc.get(i).getPrice()
							.multiply(new BigDecimal(((DiscountByAccumulation) d).getPercentage()), MathContext.DECIMAL32)));
					
					productsAcc.get(i).setPrice(temp.setScale(2, RoundingMode.CEILING));
					appliedDiscounts.append(
							(productsAcc.get(i).getName() 
									+ " " + ((DiscountByAccumulation) d).getPercentage()*100 
									+ "% off: -" + discount + "\n"));
				}
			}
		}
		
		if (appliedDiscounts.length() == 0) {
			appliedDiscounts.append("(no offers available)");
		}
	}

}