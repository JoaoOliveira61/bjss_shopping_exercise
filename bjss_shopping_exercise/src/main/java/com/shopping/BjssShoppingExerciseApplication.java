package com.shopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.shopping.*" })
@EnableJpaRepositories(basePackages = { "com.shopping.*"})    
@EntityScan(basePackages = { "com.shopping.*" })
public class BjssShoppingExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BjssShoppingExerciseApplication.class, args);
	}
}
