package com.shopping.product.service;

import java.util.List;

import com.shopping.product.entity.Product;

public interface ProductService {

	public List<Product> findAll();

	public Product findByName(String name);

	public Product findById(long id);
	
	public void save(Product product);
	
	public void deleteById(long id);
}