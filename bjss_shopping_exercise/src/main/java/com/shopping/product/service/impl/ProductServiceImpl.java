package com.shopping.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.product.entity.Product;
import com.shopping.product.repository.ProductRepository;
import com.shopping.product.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	public Product findByName(String name) {
		return productRepository.findAll().stream().filter(p -> p.getName().equals(name)).findAny().orElse(null);
	}

	@Override
	public Product findById(long id) {
		return productRepository.findOne(id);
	}

	@Override
	public void save(Product product) {
		productRepository.save(product);
	}

	@Override
	public void deleteById(long id) {
		productRepository.delete(id);
	}

}
