package com.shopping.discount.service;

import java.util.List;

import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;

public interface DiscountService {

	public List<Discount> findAll();

	public List<Discount> findByProductId(long productId);

	public Discount findById(long id);
	
	public List<Discount> findByDiscountType(DiscountType type);
	
	public void save(Discount discount);
	
	public void deleteById(long id);
	
}
