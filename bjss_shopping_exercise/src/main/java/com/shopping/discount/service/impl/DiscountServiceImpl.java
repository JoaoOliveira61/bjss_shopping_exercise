package com.shopping.discount.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;
import com.shopping.discount.repository.DiscountRepository;
import com.shopping.discount.service.DiscountService;

@Service
public class DiscountServiceImpl implements DiscountService {

	@Autowired
	private DiscountRepository discountRepository;
	
	@Override
	public List<Discount> findAll() {
		return discountRepository.findAll();
	}

	@Override
	public List<Discount> findByProductId(long productId) {
		return discountRepository.findAll().stream().filter(d -> d.getProductId() == productId).collect(Collectors.toList());
	}

	@Override
	public Discount findById(long id) {
		return discountRepository.findOne(id);
	}

	@Override
	public void save(Discount discount) {
		discountRepository.save(discount);
	}

	@Override
	public void deleteById(long id) {
		discountRepository.delete(id);
	}

	@Override
	public List<Discount> findByDiscountType(DiscountType type) {
		return discountRepository.findAll().stream().filter(d -> d.getDiscountType().equals(type.getDiscType())).collect(Collectors.toList());
	}

}
