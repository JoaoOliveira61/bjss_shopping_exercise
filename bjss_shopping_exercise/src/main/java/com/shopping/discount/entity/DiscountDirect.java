package com.shopping.discount.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;

/**
 * Represents the direct percentage discounts on a product
 * @author Joao Oliveira
 *
 */
@Entity
@DiscriminatorValue("DIR")
@Table(name="Discount_Direct")
public class DiscountDirect extends Discount {
	
	private float percentage;

	protected DiscountDirect() {}
	
	public DiscountDirect(long id, long productId, float percentage) {
		super(id, productId, DiscountType.DIRECT.getDiscType());
		this.percentage = percentage;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

}
