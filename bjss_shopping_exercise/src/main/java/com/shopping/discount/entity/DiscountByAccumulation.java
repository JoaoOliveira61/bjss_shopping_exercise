package com.shopping.discount.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;

/**
 * Represents the product discounts based on accumulation of products 
 * @author Joao Oliveira
 *
 */
@Entity
@DiscriminatorValue("ACC")
@Table(name = "Discount_By_Accumulation")
public class DiscountByAccumulation extends Discount {

	// Defines the minimum number of products required to trigger the discount
	private int productForDiscMinNum;
	// Defines the product id which will be discounted
	private long prodDiscountedId;
	// Defines the product to trigger/product to be discounted rate (example: two cans of soup leads to one bread half off)
	private int prodForProdRelation;
	private float percentage;

	protected DiscountByAccumulation() { }

	public DiscountByAccumulation(long id, long productId, int productForDiscMinNum, long prodDiscountedId, int prodForProdRelation,
			float percentage) {
		super(id, productId, DiscountType.ACCUMULATION.getDiscType());
		this.productForDiscMinNum = productForDiscMinNum;
		this.prodDiscountedId = prodDiscountedId;
		this.prodForProdRelation = prodForProdRelation;
		this.percentage = percentage;
	}

	public int getProductForDiscMinNum() {
		return productForDiscMinNum;
	}

	public void setProductForDiscMinNum(int productForDiscMinNum) {
		this.productForDiscMinNum = productForDiscMinNum;
	}

	public long getProdDiscountedId() {
		return prodDiscountedId;
	}

	public void setProdDiscountedId(long prodDiscountedId) {
		this.prodDiscountedId = prodDiscountedId;
	}

	public int getProdForProdRelation() {
		return prodForProdRelation;
	}

	public void setProdForProdRelation(int prodForProdRelation) {
		this.prodForProdRelation = prodForProdRelation;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
}
