package com.shopping.discount.model;

public enum DiscountType {
	DIRECT("DIR"),
	ACCUMULATION("ACC");
	
	private String discType;
	
	DiscountType(String discType) {
		this.setDiscType(discType);
	}

	public String getDiscType() {
		return discType;
	}

	public void setDiscType(String discType) {
		this.discType = discType;
	}
}
