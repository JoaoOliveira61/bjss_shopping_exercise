package com.shopping.controller;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

import com.shopping.basket.Basket;

@Controller
@Profile("!test")
public class CommandLineController implements CommandLineRunner{
	
	@Autowired
	Basket basket;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Price basket application instructions:"
				+ "\nAdd items to checkout basket: PriceBasket item1 item2 item3....");
		
		Scanner scanner = new Scanner(System.in);
		String[] commandLine = scanner.nextLine().split("\\s+");

		if (!commandLine[0].equals("PriceBasket")) {
			System.out.println("Invalid instruction.");
		} else {
			for (String product: commandLine) {
				basket.addProduct(product);
			}
			
			basket.calculateTotals();
			System.out.println("Subtotal: " + basket.getSubTotal());
			System.out.println(basket.getAppliedDiscounts());
			System.out.println("Total: " + basket.getTotal());
		}

		scanner.close();
	}
}
