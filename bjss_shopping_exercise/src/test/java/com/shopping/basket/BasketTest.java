package com.shopping.basket;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BasketTest {

	@Autowired
	Basket basket;
	
	private final static String PRODUCT_NAME_1 = "Apples";
	private final static String PRODUCT_NAME_2 = "Milk";
	private final static String PRODUCT_NAME_3 = "Carrots";
	private final static String PRODUCT_NAME_4 = "Soup";
	private final static String PRODUCT_NAME_5 = "Bread";
	
	@Test
	public void testAddExistingProduct() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_1);
		
		assertFalse(basket.getProductsInBasket().isEmpty());
		assertEquals(basket.getProductsInBasket().get(0).getName(), PRODUCT_NAME_1);
	}
	
	@Test
	public void testAddNonExistingProduct() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_3);
		
		assertTrue(basket.getProductsInBasket().isEmpty());
	}
	
	@Test
	public void testCalcTotalsNoDiscounts() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_2);
		basket.addProduct(PRODUCT_NAME_4);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("1.95"));
		assertEquals(basket.getTotal(), new BigDecimal("1.95"));
	}
	
	@Test
	public void testCalcTotalsDirectDisc() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_1);
		basket.addProduct(PRODUCT_NAME_2);
		basket.addProduct(PRODUCT_NAME_5);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("3.10"));
		assertEquals(basket.getTotal(), new BigDecimal("3.00"));
	}
	
	@Test
	public void testCalcTotalsAccDiscount() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_5);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("2.10"));
		assertEquals(basket.getTotal(), new BigDecimal("1.70"));
	}
	
	@Test
	public void testCalcTotalsAccDiscountMultiple() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_5);
		basket.addProduct(PRODUCT_NAME_5);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("4.20"));
		assertEquals(basket.getTotal(), new BigDecimal("3.40"));
	}
	
	@Test
	public void testCalcTotalsAccDiscountOne() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_5);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("3.40"));
		assertEquals(basket.getTotal(), new BigDecimal("3.00"));
	}
	
	@Test
	public void testCalcTotalsAccDiscountNotApplicable() {
		basket.clearBasket();
		basket.addProduct(PRODUCT_NAME_4);
		basket.addProduct(PRODUCT_NAME_5);
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("1.45"));
		assertEquals(basket.getTotal(), new BigDecimal("1.45"));
	}
	
	@Test
	public void testCalcTotalsNoProducts() {
		basket.clearBasket();
		
		basket.calculateTotals();
		
		assertEquals(basket.getSubTotal(), new BigDecimal("0"));
		assertEquals(basket.getTotal(), new BigDecimal("0"));
	}
	
	
}
