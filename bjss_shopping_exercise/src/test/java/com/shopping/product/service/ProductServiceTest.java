package com.shopping.product.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.shopping.product.entity.Product;
import com.shopping.product.service.impl.ProductServiceImpl;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProductServiceTest {

	private final static long PRODUCT_ID_1 = 4;
	private final static String PRODUCT_NAME_1 = "Apples";
	private final static long PRODUCT_ID_2 = 10;
	private final static String PRODUCT_NAME_2 = "Aples";
	private final static long PRODUCT_ID_3 = 6;
	private final static String PRODUCT_NAME_3 = "Carrots";
	private final static BigDecimal PRODUCT_PRICE_3 = new BigDecimal(0.75);
	private Product testProduct;
	private Product testProduct1;
	private List<Product> productList;
	
	@Before
	public void before() {
		testProduct1 = new Product(PRODUCT_ID_3, PRODUCT_NAME_3, PRODUCT_PRICE_3);
	}
	
	@Autowired
	private ProductServiceImpl productService;
	
	@Test
	public void testGetExistingProduct() {
		testProduct = productService.findById(PRODUCT_ID_1);
		
		assertNotNull(testProduct);
		assertEquals(testProduct.getId(), PRODUCT_ID_1);
	}
	
	@Test
	public void testGetUnexistingProduct() {
		testProduct = productService.findById(PRODUCT_ID_2);
		
		assertNull(testProduct);
		
	}
	
	@Test
	public void testFindByExistingName() {
		testProduct = productService.findByName(PRODUCT_NAME_1);
		
		assertNotNull(testProduct);
		assertEquals(testProduct.getName(), PRODUCT_NAME_1);
	}
	
	@Test
	public void testFindByNotExistingName() {
		testProduct = productService.findByName(PRODUCT_NAME_2);
		
		assertNull(testProduct);
	}
	
	@Test
	public void testFindAll() {
		productList = productService.findAll();
		
		assertNotNull(productList);
		assertEquals(productList.size(), 5);
	}
	
	@Test
	public void testSave() {
		productService.save(testProduct1);
		productList = productService.findAll();
		
		assertNotNull(productList);
		assertEquals(productList.size(), 6);
		assertTrue(productList.contains(testProduct1));
	}
	
	@Test
	public void testDeleteById() {
		productList = productService.findAll();	
		assertNotNull(productList);
		assertEquals(productList.size(), 6);
		
		productService.deleteById(PRODUCT_ID_3);
		productList = productService.findAll();	
		
		assertEquals(productList.size(), 5);
		assertFalse(productList.contains(testProduct1));
	}
}
