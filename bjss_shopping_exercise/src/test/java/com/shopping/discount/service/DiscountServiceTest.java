package com.shopping.discount.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.shopping.discount.entity.DiscountByAccumulation;
import com.shopping.discount.entity.DiscountDirect;
import com.shopping.discount.model.Discount;
import com.shopping.discount.model.DiscountType;
import com.shopping.discount.service.impl.DiscountServiceImpl;

import static org.junit.Assert.assertTrue;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DiscountServiceTest {

	private final static long DISCOUNT_ID_1 = 1;
	private final static long DISCOUNT_ID_2 = 10;
	private final static long DISCOUNT_ID_3 = 3;
	private final static long DISCOUNT_ID_4 = 4;
	private final static long PRODUCT_ID_1 = 4;
	private Discount testDiscount;
	private List<Discount> testDiscountList;
	private DiscountDirect testDiscount1;
	private DiscountByAccumulation testDiscount2;

	@Before
	public void before() {
		testDiscount1 = new DiscountDirect(DISCOUNT_ID_3, 2, 0.35f);
		testDiscount2 = new DiscountByAccumulation(DISCOUNT_ID_4, 3, 5, 2, 1, 0.3f);
	}

	@Autowired
	private DiscountServiceImpl discountService;

	@Test
	public void testGetExistingDirectDiscount() {
		testDiscount = discountService.findById(DISCOUNT_ID_1);

		assertNotNull(testDiscount);
		assertEquals(testDiscount.getId(), DISCOUNT_ID_1);
	}

	@Test
	public void testGetUnexistingDiscount() {
		testDiscount = discountService.findById(DISCOUNT_ID_2);

		assertNull(testDiscount);

	}

	@Test
	public void testFindByExistingProductId() {
		testDiscountList = discountService.findByProductId(PRODUCT_ID_1);

		assertNotNull(testDiscountList);
		assertEquals(testDiscountList.size(), 1);
		assertEquals(testDiscountList.get(0).getId(), DISCOUNT_ID_1);
	}

	@Test
	public void testFindByDirectDiscountType() {
		testDiscountList = discountService.findByDiscountType(DiscountType.DIRECT);

		assertNotNull(testDiscountList);
		for (Discount d : testDiscountList) {
			assertEquals(d.getDiscountType(), "DIR");
		}
	}

	@Test
	public void testFindByAccumulativeDiscountType() {
		testDiscountList = discountService.findByDiscountType(DiscountType.ACCUMULATION);

		assertNotNull(testDiscountList);
		for (Discount d : testDiscountList) {
			assertEquals(d.getDiscountType(), "ACC");
		}
	}

	@Test
	public void testFindAll() {
		testDiscountList = discountService.findAll();
		
		assertNotNull(testDiscountList);
		assertEquals(testDiscountList.size(), 4);
	}

	@Test
	public void testSaveDiscountDirect() {
		discountService.save(testDiscount1);
		
		testDiscountList = discountService.findByDiscountType(DiscountType.DIRECT);

		assertNotNull(testDiscountList);
		assertEquals(testDiscountList.size(), 2);
		for (Discount d : testDiscountList) {
			assertEquals(d.getDiscountType(), "DIR");
		}
	}
	
	@Test
	public void testSaveDiscountAccumulative() {
		discountService.save(testDiscount2);
		
		testDiscountList = discountService.findByDiscountType(DiscountType.ACCUMULATION);

		assertNotNull(testDiscountList);
		assertEquals(testDiscountList.size(), 2);
		for (Discount d : testDiscountList) {
			assertEquals(d.getDiscountType(), "ACC");
		}
	}

	@Test
	public void testDeleteById() {
		testDiscountList = discountService.findAll();	
		assertNotNull(testDiscountList);
		assertEquals(testDiscountList.size(), 4);
		
		discountService.deleteById(DISCOUNT_ID_4);
		testDiscountList = discountService.findAll();	
		
		assertEquals(testDiscountList.size(), 3);
		assertFalse(testDiscountList.contains(testDiscount2));
	}

}
